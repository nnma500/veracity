# VERACITY

VERACITY is a tool-supported iterative approach for the efficient and accurate verification of nonfunctional requirements under epistemic parameter uncertainty. It supports both the verification of new system designs, and the verification of planned updates to existing systems.
Further details at this website (https://www.cs.york.ac.uk/tasp/VERACITY/index.htm).
