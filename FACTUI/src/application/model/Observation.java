package application.model;

import java.util.ArrayList;

public class Observation {

	public String name;
	public String type;
	
	private ArrayList<Integer> instances = null;
	public String error = "";

	Observation() {
		instances = new ArrayList<Integer>();
	}

	/*
	 * Create an observation object from an instantiating string of the form //
	 * param: x 3414 2 2547 This means there are three parameters x1,x2,x3 and they
	 * have been observed 3414, 2 and 2547 times each.
	 * 
	 * A return value of false indicates that the parsing failed
	 */

	public Boolean initialise(String sInit) {
		int counter = 0;
		int length = 0;
		sInit = sInit.trim();
		if (sInit.endsWith(";")) {
			sInit = sInit.replaceAll(";", "");
			sInit = sInit.replaceAll("=", "");
		} else {
			error = "Param parse error: No ;";
			return false;
		}

		if (sInit.startsWith("param")) {
			sInit = sInit.substring(5).trim();
			String[] spVal = sInit.split("\\s+\\s*?");

			counter = 0;
			for (String s : spVal) {
				if (s.matches("component"))
					break;
				counter++;
			}
			length = counter;
			if (length < length - 3) {
				error = "Param parse error: format incorrect";
				return false;
			}
			this.name = spVal[0];
			this.type = "double";
			for (int i = 3; i < length; i++)
				this.add(Integer.parseInt(spVal[i].trim()));

			return true;
		} else
			return false;

	}

	// Note that the order in which counts are added are important.
	public void add(int count) {
		instances.add(count);
	}

	// return the number of parameters in this namespace.
	public int getSize() {
		return instances.size();
	}

	public int getCount(int which) {
		return (int) instances.get(which);
	}

	public int getParamByIndex(int index) {
		return (int) instances.get(index);
	}

	public int getTotal() {
		int total = 0;
		for (int i = 0; i < instances.size(); i++) {
			total += (int) instances.get(i);
		}
		return total;
	}

}
