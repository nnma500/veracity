package testingHeuristic;


public class Requirement {
	public String originalReq;       // required as read from the requirements file
	public String propertyPCTL;     // PCTL formula for this property
	public String property;         // expression for the property involved in this requirement, e.g. "x1+0.5*y1" and "0.2x1-z1"
	public double threshold;        // threshold that the property must not exceed or must be under 
	public boolean belowThreshold;  // true if the property must be below the threshold; false if the property must be above it
	public double lowerBound;       // lower bound of current confidence interval for the property
	public double upperBound;       // upper bound of current confidence interval for the property
	public boolean satisfied;       // true if this requirement is satisfied
	
	public Requirement(String originalReq, String propertyPCTL, String property, double threshold, boolean belowThreshold, double lowerBound, double upperBound) {
		this.originalReq = originalReq;
		this.propertyPCTL = propertyPCTL;
		this.property = property;
		this.threshold = threshold;
		this.belowThreshold = belowThreshold;
		this.lowerBound = lowerBound;
		this.upperBound = upperBound;
		this.satisfied = false;
	}
}
