package testingHeuristic;

import java.util.Hashtable;
import org.mariuszgromada.math.mxparser.*;

public class TestHeuristic {

	private static double violationMarginEpsilon = 0.85;

	// The method computes and returns an array of component "importance" values
	// associated with a property (algebraic expression
	// whose variables are parameters of the components of system); testing
	// components with higher "importance" to observe their
	// behaviour is more beneficial to narrowing the confidence interval for the
	// property than testing components with lower "importance"
	// - property = the actual expression to be analysed, e.g. "x1+0.5*y1"
	// - nComponents = number of components to analyse
	// - parameters = dictionary mapping parameter names (e.g. "x1") to information
	// about parameters (estimate value, ID of component
	// associated with it)
	private double[] computeImportance(String property, int nComponents, Hashtable<String, Parameter> parameters) {
		// 1. Initialise importance factor of components
		double[] componentImportance = new double[nComponents];
		for (int i = 0; i < nComponents; i++) {
			componentImportance[i] = 0;
		}

		// 2. Build argument array
		Argument[] arguments = new Argument[parameters.size()];
		int i = 0;
		for (String paramName : parameters.keySet()) {
			arguments[i++] = new Argument(paramName + " = " + parameters.get(paramName).value);
		}

		// 3. Establish the importance of each parameter, one parameter at a time
		System.out.println("  -- Importance factors for property " + property);
		parameters.forEach((paramName, param) -> {
			String derivProperty = "der(" + property + "," + paramName + ")";
			Expression e = new Expression(derivProperty, arguments);
			double importance = java.lang.Math.abs(e.calculate());
			componentImportance[param.componentID] += importance;
			System.out.println("     " + paramName + " has importance " + importance + " (added to component "
					+ (param.componentID + 1) + ")");
		});

		// 4. Return importance values for this property
		return componentImportance;
	}

	// Compute number of additional tests/observations to be made of the system
	// components; or return null if all requirements
	// are satisfied or one requirement is violated; the arguments are:
	// - requirements = the requirements containing the actual expressions to be
	// analysed, e.g. "x1+0.5*y1" and "0.2x1-z1"
	// - costs = array telling the cost of testing each component to obtain one more
	// observation
	// - parameters = dictionary mapping parameter names (e.g. "x1") to information
	// about parameters (estimate value, ID of component
	// associated with)
	// - budget = how much testing cost should be used
	// - firstRound = true if the method is called to decide the numbers of
	// first-ever observations of these components
	public int[] computeObservations(Requirement[] requirements, double[] costs,
			Hashtable<String, Parameter> parameters, int budget, boolean firstRound, boolean uniformTesting) {
		// 0. Is this the first round of observations ever?
		if (firstRound) {
			System.out.println("** Requirements not checked in round 1");
			return computeUniformObservations(costs, budget);
		}

		// 1. Check whether:
		// - the requirements are all satisfied;
		// - there is a violated requirement (so the requirements as a whole cannot be
		// met);
		// - or (otherwise) if any requirement is likely to be violated - and remember
		// which one is most likely to be violated
		int nSatisfiedRequirements = 0;
		int worstRequirementIndex = -1; // index of requirement most likely to be violated (if any)
		double worstViolationMargin = 1; // measure of likelihood that the "worst" requirement is actually violated

		// 1.1. Examine each requirement
		System.out.println("** Checking requirements");
		for (int i = 0; i < requirements.length; i++) {
			Requirement req = requirements[i];
			if ((req.belowThreshold && req.upperBound <= req.threshold)
					|| (!req.belowThreshold && req.lowerBound >= req.threshold)) {
				System.out.println("  -- Requirement " + req.originalReq + " is satisfied: [" + req.lowerBound + ","
						+ req.upperBound + "] is " + (req.belowThreshold ? "below" : "above") + " " + req.threshold);
				req.satisfied = true;
				nSatisfiedRequirements++;
			} else if ((req.belowThreshold && req.lowerBound >= req.threshold)
					|| (!req.belowThreshold && req.upperBound <= req.threshold)) {
				System.out.println("  -- Requirement " + req.originalReq + " is violated: [" + req.lowerBound + ","
						+ req.upperBound + "] is not " + (req.belowThreshold ? "below" : "above") + " "
						+ req.threshold);
				System.out
						.println("** Further testing is not required: the set of requirements as a whole is violated");
				return null; // this requirement is violated, so no need to carry on
			} else {
				System.out.println("  -- Requirement " + req.originalReq + " is undecidable: " + req.threshold
						+ " is inside [" + req.lowerBound + "," + req.upperBound + "]");
				double d1 = req.upperBound - req.threshold;
				double d2 = req.threshold - req.lowerBound;
				double margin = req.belowThreshold ? d2 / d1 : d1 / d2;
				// System.out.println("\n\n %%%%%% Does margin "+margin+" < " + (1 -
				// violationMarginEpsilon) +" ? %%%%%%\n");
				if (margin < 1 - violationMarginEpsilon) {
					// this requirement is likely to be violated
					if (margin < worstViolationMargin) {
						// so far, this is the requirement most likely to be violated
						worstViolationMargin = margin;
						worstRequirementIndex = i;
					}
				}
			}
		}

		// 1.2. Are all requirements satisfied?
		if (nSatisfiedRequirements == requirements.length) {
			System.out.println("** Further testing is not required: all requirements are satisfied");
			return null; // all requirements are satisfied, so no need to carry on
		}

		// 1.3. Handle uniformTesting
		if (uniformTesting) {
			return computeUniformObservations(costs, budget);
		}

		// 2. Initialise importance factor of components - further testing is needed
		int nComponents = costs.length;
		double[] componentImportance = new double[nComponents];
		for (int i = 0; i < nComponents; i++) {
			componentImportance[i] = 0;
		}

		// 3. Is there any requirement that is likely to be violated? If so, focus all
		// testing on only this requirement.
		if (worstRequirementIndex != -1) {
			double[] importance = computeImportance(requirements[worstRequirementIndex].property, nComponents,
					parameters);
			for (int j = 0; j < nComponents; j++) {
				componentImportance[j] += importance[j];
			}
		}

		// 4. Else (i.e. if no requirement is clearly likely to be violated), perform
		// further testing taking all requirements
		// not known to be satisfied into account
		else {
			for (int i = 0; i < requirements.length; i++) {
				Requirement req = requirements[i];

				// 4.1. Skip satisfied requirement
				if (req.satisfied) {
					continue;
				}

				// 4.2. Obtain component importance values specific to the property from this
				// requirement
				double[] importance = computeImportance(req.property, nComponents, parameters);

				// 4.2. Compute weight for this requirement
				double d1 = req.upperBound - req.threshold;
				double d2 = req.threshold - req.lowerBound;
				double diff = Math.abs(d1 - d2);
				if (diff < 0.000001) { // avoid division by zero
					diff = 0.000001;
				}
				double weight = (d1 + d2) / diff; // the closer d1 and d2 are, the more testing is needed

				for (int j = 0; j < nComponents; j++) {
					componentImportance[j] += weight * importance[j];
				}
			}
		}

		// 5. Compute number of new observations
		double totalImportance = 0;
		int[] newObservations = new int[nComponents];
		for (int i = 0; i < nComponents; i++) {
			totalImportance += componentImportance[i];
		}
		for (int i = 0; i < nComponents; i++) {
			double budgetForThisComponent = ((budget * componentImportance[i]) / totalImportance);
			newObservations[i] = (int) (budgetForThisComponent / costs[i]);
		}

		// 6. Return number of observations
		return newObservations;
	}

	public int[] computeUniformObservations(double[] costs, double budget) {
		int nComponents = costs.length;
		int[] newObservations = new int[nComponents];
		for (int i = 0; i < nComponents; i++) {
			newObservations[i] = (int) (budget / (nComponents * costs[i]));
		}
		return newObservations;
	}
}
