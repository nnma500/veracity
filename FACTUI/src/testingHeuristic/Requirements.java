package testingHeuristic;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Requirements {
	private ArrayList<Requirement> reqs;
	private Requirement requirementArray[] ;
	public Requirements(String reqsFile) throws FileNotFoundException {
		reqs = new ArrayList<Requirement>(); 
		Scanner scanner = new Scanner(new File(reqsFile));
		double threshold = 0;
		String line = null;
		while (scanner.hasNextLine()) {
			
	if (!(line = scanner.nextLine()).isEmpty()) {
		String line1 = line.substring(line.indexOf("[")+1,line.indexOf("]"));
		String originalReq = new String(line);
		line=line.replace(line1, "");
			int pos1 = line.indexOf("<=");
			int pos2 = line.indexOf("<");
			int pos3 = line.indexOf(">=");
			int pos4 = line.indexOf(">");
			boolean belowThreshold = (pos1!=-1 || pos2!=-1);
		    int thresholdStartIndex = (pos1!=-1) ? pos1+2 : ((pos2!=-1) ? pos2+1 : ((pos3!=-1)? pos3+2 : pos4+1));
			int thresholdEndIndex = line.indexOf('[', thresholdStartIndex);

			
			String propertyPCTL= line;
			line1="["+line1+"]";
			if (thresholdEndIndex == -1) {
			
			//	System.out.println("propertyPCTL: "+ propertyPCTL);
				threshold = Double.parseDouble(line.substring(thresholdStartIndex,line.length()));
				propertyPCTL= getOldFormat(propertyPCTL,thresholdStartIndex ,line1 );

			}
			else {
			propertyPCTL= getOldFormat(propertyPCTL,thresholdStartIndex ,line1 );
					// System.out.println("propertyPCTL old: "+propertyPCTL);
					 threshold = Double.parseDouble(line.substring(thresholdStartIndex,thresholdEndIndex));
					// System.out.println("threshold: "+threshold);
				}
			
			Requirement r = new Requirement(originalReq, propertyPCTL, "", threshold, belowThreshold, 0, 0); 
			reqs.add(r);
		}}
		scanner.close();
		requirementArray = new Requirement[getRequirements().size()];
		ArraySetter();
	}

	
	
	public String getOldFormat(String pctl, int fromIndex, String between) {
		//line=line.replace("[]", line1);
		String part1="", part2 ="";
		if (pctl.contains("=?")) {
			//System.out.println("New pctl: "+pctl);
			pctl=pctl.substring(0,pctl.indexOf("["));
		pctl=pctl+between;
		}
		else {

		part1 = pctl.substring(0, fromIndex -1 );
		part2 = between;
		pctl=part1+" =? "+part2;
		}

		return pctl;
	}	
	public ArrayList<Requirement> getRequirements() {
		return reqs;
	}
	//this method to get the requirement in one dimensional array
	public Requirement[] getTheRequirements() {
		return requirementArray;
	}
	//this method to fill the requirement array
	public void ArraySetter() {
		for(int i=0;i<reqs.size();i++)
		requirementArray[i]=reqs.get(i);
	}
	public double getCIvalFromPCTL(String pctl) {
		pctl = pctl.trim();
		pctl = pctl.replaceAll("\\s+\\s*?", "");
	//	System.out.println(pctl);
		int startIndex = 0;
		int pos1 = pctl.indexOf(">");
		int pos2 = pctl.indexOf("<");
		int pos3 = pctl.indexOf("<=");
		int pos4 = pctl.indexOf(">=");

		if (pos1 != -1)
			startIndex = pctl.indexOf(">");

		if (pos2 != -1)
			startIndex = pctl.indexOf("<");

		if (pos3 != -1)
			startIndex = pctl.indexOf("=");

		if (pos4 != -1)
			startIndex = pctl.indexOf("=");

		int endIndex = pctl.indexOf("[");

		pctl = pctl.substring(startIndex + 1, endIndex);
		if (pctl.length() > 4 | pctl.length() < 3) {
			System.err.println("Please check the PCTL syntax, there is more than 2 digits");
			System.exit(1);
		}
		return Double.parseDouble(pctl) * 100;
	}
}
