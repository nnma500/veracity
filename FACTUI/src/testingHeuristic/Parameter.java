package testingHeuristic;

import java.util.ArrayList;

public class Parameter {
  public double value;
  public int componentID;
	private String name;
	private double cost;
	private int totalObservations;
	private String paramLines = null;
	private ArrayList<String> names = new ArrayList<String>();
	private ArrayList<Integer> observations = new ArrayList<Integer>();
  
  public Parameter(String n) {
		this.name = n;
		clearArrayLists();
		this.totalObservations = 0;
	}
  
  public Parameter (double value, int componentID){
	  this.value = value;
	  this.componentID = componentID;
  }
  public double getValue() {
	  return this.value;
  }
  
  /*public void setValue(double value) {
	  this.value = value;
  }*/
  
  public int getComponentID() {
	  return this.componentID;
  } 
  
  public void setID(int id) {
		this.componentID = id;
	}
  public String getName() {
		return name;
	}

	public void setNames(String name) {
		this.names.add(name);
	}

	public ArrayList<String> getNames() {
		return names;
	}

	public void setObservations(int observation) {
		this.observations.add(observation);
	}

	public ArrayList<Integer> getObservations() {
		return observations;
	}

	public void setCost(double cost) {
		this.cost = cost;
	}

	public double getCost() {
		return cost;
	}

	public void setParamLines(String line) {
		this.paramLines = line;
	}

	public String getParamLines() {
		return paramLines;
	}

	public int getTotalObservations() {
		if (this.totalObservations == 0) {
			for (int i = 0; i < observations.size(); i++)
				this.totalObservations += observations.get(i);
		}
		return totalObservations;
	}
  
  private void clearArrayLists() {
		this.names.clear();
		this.observations.clear();
	}
}
