package veracity;

import application.model.ExperimentData;
import application.model.Settings;
import confidenceIntervalCalculation.ConfidenceInterval;
import framework.FactFramework;
import inputOutput.Time;
import logging.FACTLogger;
import mathEngine.MatLabEngine;
import parametricModelChecking.OperatingSystem;
import parametricModelChecking.PrismParametricModelChecker;

public class Execute {

	private ExperimentData data = null;
	private FACTLogger flog = new FACTLogger();

	public Execute(ExperimentData _data, FACTLogger _flog) {
		data = _data;
		flog = _flog;
	}

	public String run(MatLabEngine e) {
		// data.flog.newLog();
		flog.add("\n\n***********Run called***********\n\n");
		data.running = true;


		data.timer = new Time(); // Used for experimental diagnostics

		FactFramework fact = data.fact; // Create a FACT framework
		// read the settings file to get the location of PRISM
		Settings settings = new Settings();
		settings.read();

		if (settings.get("PRISMLocation") != null) {
			ExperimentData.PRISMLOCATION = settings.get("PRISMLocation");
		}

		fact.setPpmc(new PrismParametricModelChecker(ExperimentData.PRISMLOCATION, OperatingSystem.MAC));
		fact.setMathsEngine(e);

		// initialise the engine
		data.populateFACT();
		data.fInput.ciResults.clear();
		fact.resetSimultaneousConfidenceIntervalCalculator(data.fInput);
		data.flog.add("Prism is attempting parametric model checking");
		data.fInput.algebraicExpression = fact.getPpmc().modelCheck(data.fInput.model, data.fInput.property, data.flog);

		// Calculate the confidence interval and report it.
		fact.nextStepCalculateConfidenceInterval(data.fInput);
		ConfidenceInterval ci = fact.getLastComputedCI();

		String result = fact.getLastComputedCI().toString();
		data.flog.add(result);

		data.timer.stop();
		data.flog.add("Time to calculate: " + data.fInput.timing.toString() + "s");

		String sPoly = data.fInput.algebraicExpression.poly;
		if (sPoly.length() == 0) {
			data.flog.add("Prism failed to return a polynomial");
			data.running = false;
			return "";
		} else
			data.flog.add("Model checking complete");

		data.flog.add("-----------------------------------------------------");
		data.flog.add("Polynomial sent to Matlab:");
		data.flog.add(sPoly);
		data.flog.add("Calculated in " + data.timer + " seconds");
		data.flog.add("-----------------------------------------------------");
		data.flog.add("Hill Climbing Max Iterations: " + data.fInput.config.MAX_ITERATIONS);

		return result;
	}
}
