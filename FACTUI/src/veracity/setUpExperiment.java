package veracity;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import testingHeuristic.Parameter;

public class setUpExperiment {
	private ArrayList<Parameter> param = new ArrayList<>();
	private ArrayList<Integer> paramID = new ArrayList<>();

	public ArrayList<Parameter> getParam() {
		return param;
	}

	public ArrayList<Integer> getParamID() {
		return paramID;
	}

	// used to extract param characteristics and store them in param

	public setUpExperiment(String modelCode) throws FileNotFoundException {
		InputStream is = new ByteArrayInputStream(modelCode.getBytes());
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		paramName(modelCode);
		String testParam = "^\\s*param\\s+\\w+\\s+:\\s+observations\\s*=(\\s+\\d+){1,}\\s*?;"
				+ "\\s+component\\s*=(\\s+\\d+)\\s*?;\\s*?"
				+ "(\\s+cost\\s*=(\\s+[+]?\\d*\\.\\d+)(?![+0-9\\.])\\s*?;\\s*?)?";
		String line;
		int lineNumber = 0;
		int counter = 0;
		try {
			while ((line = br.readLine()) != null) {
				if (line.matches(testParam)) {
					this.param.get(lineNumber).setParamLines(line);
					String[] spVal = line.trim().replace("=", " ").replace(";", "").split("\\s+\\s*?");
					counter = 0;
					for (String s : spVal) {
						if (s.matches("component"))
							break;
						counter++;
					}

					if (counter < counter - 3) {
						System.err.println("Param parse error: format incorrect");
					}
					for (int i = 1; i <= counter - 4; i++) {
						this.param.get(lineNumber).setNames(spVal[1] + i);
						this.param.get(lineNumber).setObservations(Integer.parseInt(spVal[3 + i]));
					}
					this.param.get(lineNumber).setID(Integer.parseInt(spVal[counter + 1]));
					this.paramID.add(Integer.parseInt(spVal[counter + 1]));
					this.param.get(lineNumber)
							.setCost((spVal.length - counter - 2 == 0) ? 1.0 : Double.parseDouble(spVal[counter + 3]));
					lineNumber++;
				}
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// this method to initialise param arraylist with required param
	public void paramName(String model) throws FileNotFoundException {
		InputStream is = new ByteArrayInputStream(model.getBytes());
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		String testParam = "^\\s*param\\s+\\w+\\s+:\\s+observations\\s*=(\\s+\\d+){1,}\\s*?;"
				+ "\\s+component\\s*=(\\s+\\d+)\\s*?;\\s*?"
				+ "(\\s+cost\\s*=(\\s+[+]?\\d*\\.\\d+)(?![+0-9\\.])\\s*?;\\s*?)?";
		this.param.clear();
		this.paramID.clear();
		String line;
		int counter = 0;
		try {
			while ((line = br.readLine()) != null) {
				if (line.matches(testParam)) {
					String[] spVal = line.trim().replace("=", " ").replace(";", "").split("\\s+\\s*?");
					counter = 0;
					for (String s : spVal) {
						if (s.matches("component"))
							break;
						counter++;
					}
					this.param.add(new Parameter(spVal[1]));
				}
			}

			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
