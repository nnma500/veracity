
package veracity;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.Hashtable;
import application.model.ExperimentData;
import application.model.Settings;
import logging.FACTLogger;
import mathEngine.MatLabEngine;
import testingHeuristic.Parameter;
import testingHeuristic.Requirement;
import testingHeuristic.Requirements;
import testingHeuristic.TestHeuristic;

import java.util.Scanner;

public class mainVERACITY {
	private static FACTLogger flog = new FACTLogger();
	private static int roundBudget = 5000; // default value
	private static int budget = 50000; // default value
	private static String observationScript = null; // default value
	private static boolean useUniform = false; // if true, use uniform testing throughout
	private static boolean skipSatisfiedReqs = true; // if true, skip computing confidence intervals for properties
														// associated with satisfied requirements
	static TestHeuristic heuristic = new TestHeuristic();
	private static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) throws Exception {
		flog.newLog();
		if (args.length < 3) {
			System.err.println(
					"Incorrect syntax - expected 3 parameters: VERACITYT \"modelfile.pm\" \"requirementFile.pctl\" confidenceLevel\n");
			System.exit(1);
		}
		String[] arg = { "Model Path: ", "PCTL Path: ", "Confidence Level: " };
		int argCounter = 0;
		for (String s : args) {
			// System.out.println("arg : " + s);
			System.out.println(arg[argCounter] + s);
			argCounter++;
		}
		ExperimentData data = new ExperimentData(flog);
		File fModel = new File(args[0]);
		data.loadModel(fModel);
		setUpExperiment setup = new setUpExperiment(data.getModelCode());
		//setup.setUpExperiment(data.getModelCode());

		// read the settings file
		Settings settings = new Settings();
		settings.read();

		// Read extra testing parameters: roundBudget, budget, observationScript,
		// useUniform and skipSatisfiedRequiements
		String roundBudgetString = settings.get("testingRoundBudget");
		if (roundBudgetString != null) {
			roundBudget = Integer.parseInt(roundBudgetString);
		}
		String budgetString = settings.get("testingBudget");
		if (budgetString != null) {
			budget = Integer.parseInt(budgetString);
		}
		observationScript = settings.get("NewObservationScriptLocation");
		String useUniformString = settings.get("useUniform");
		if (useUniformString != null) {
			useUniform = useUniformString.equals("true");
		}
		String skipSatisfiedReqsString = settings.get("skipSatisfiedReqs");
		if (skipSatisfiedReqsString != null) {
			skipSatisfiedReqs = skipSatisfiedReqsString.equals("true");
		}
		System.out.println("\n**** Using " + (useUniform ? "uniform" : "targeted") + " additional testing, "
				+ (skipSatisfiedReqs ? "skippinq" : "analysing") + " properties for satisfied requirements");

		// to store the cost of each components in an array
		double[] cost = new double[setup.getParam().size()];
		for (int i = 0; i < setup.getParam().size(); i++)
			cost[i] = setup.getParam().get(i).getCost();

		// build hash table
		Hashtable<String, Parameter> parameters = new Hashtable<String, Parameter>();
		double division;
		for (int i = 0; i < setup.getParam().size(); i++) {
			for (int a = 0; a < setup.getParam().get(i).getObservations().size(); a++) {
				division = ((double) setup.getParam().get(i).getObservations().get(a)
						/ (double) setup.getParam().get(i).getTotalObservations());
				parameters.put(setup.getParam().get(i).getNames().get(a),
						new Parameter(division, setup.getParam().get(i).getComponentID() - 1)); // parameter.get(i));
			}
		}

		Requirements reqs = new Requirements(args[1]);
		float confidence = Float.valueOf(args[2].trim()).floatValue();
		data.setconfidenceValue(confidence, confidence, 1.0f);
		MatLabEngine engine = new MatLabEngine();
		boolean firstRound = true;
		//TestHeuristic heuristic = new TestHeuristic();
		int roundCounter = 1;
		while (true) {
			System.out.println("\n**** Round " + roundCounter + " ****");
			System.out.println("** Parameters for the round:");
			for (String key : parameters.keySet()) {
				System.out.println("  -- " + key + " with estimate value " + parameters.get(key).value
						+ " from component " + (parameters.get(key).componentID + 1));
			}

			System.out.println("** Computing confidence intervals:");

			// Run FACT for every property that appears in a requirement
			for (Requirement req : reqs.getRequirements()) {
				if (!skipSatisfiedReqs || !req.satisfied) {
					data.setEvalProp(req.propertyPCTL);
					Execute fe = new Execute(data, flog);
					String result = fe.run(engine);

					String interval = result.substring(result.indexOf('[') + 1);
					String lowerBound = interval.substring(0, interval.indexOf(','));
					String upperBound = interval.substring(result.indexOf(','), interval.indexOf(']'));
					// System.out.println("interval: "+interval);
					// System.out.println("lowerBound: "+lowerBound);
					// System.out.println("upperBound: "+upperBound);
					req.lowerBound = Double.parseDouble(lowerBound);
					req.upperBound = Double.parseDouble(upperBound);
					req.property = data.fInput.algebraicExpression.toString();

					System.out.println("  -- Property: " + req.property);
					System.out.println("     Result: " + result);
				} else {
					System.out.println("  -- Unchanged property: " + req.property);
					System.out.println("     Latest result: [" + req.lowerBound + "," + req.upperBound + "]");
				}
			}

			// Find how many additional observations are necessary for each component
			int[] nAdditionalObservations = heuristic.computeObservations(reqs.getTheRequirements(), cost, parameters,
					roundBudget, firstRound, useUniform);
			firstRound = false;

			// Done if no additional observations are required
			if (nAdditionalObservations == null) {
				System.out.println("Completed session with budget " + ((roundCounter - 1) * roundBudget));
				break;
			}

			// Done if no budget left
			if (budget <= roundCounter * roundBudget) {
				System.out.println("Could not complete session before exhausting budget " + budget);
				break;
			}

			// Obtain additional observations and update model
			System.out.println("** Additional observations are required");
			for (int i = 0; i < nAdditionalObservations.length; i++) {
				System.out.println("  -- Additional observations required for component " + (i + 1) + ": "
						+ nAdditionalObservations[i]);
			}
			updateModel(data, nAdditionalObservations, parameters);

			// Increment counter
			roundCounter++;
		}

		data.fact.terminate();
		engine.stop();
		sc.close();
		System.out.println("\nRun is completed!");
		System.exit(1);
	}

	static private void updateModel(ExperimentData data, int[] nAdditionalObservations,
			Hashtable<String, Parameter> parameters) throws Exception {
		String lines[] = data.getModelCode().split("\\r?\\n");
		String updatedModel = "";

		for (String line : lines) {
			if (line.trim().startsWith("param")) {
				line = line.replaceAll(";", " ;");
				String[] atoms = line.trim().split("\\s+");
				// find component ID
				int componentIdx;
				for (componentIdx = 0; !atoms[componentIdx].equals("component"); componentIdx++)
					;
				int componentID = Integer.parseInt(atoms[componentIdx + 2]) - 1;
				int observationsIdx;
				for (observationsIdx = 0; !atoms[observationsIdx].equals("observations"); observationsIdx++)
					;
				int nObservations = componentIdx - observationsIdx - 3;
				int[] newObservations = new int[nObservations];

				if (nAdditionalObservations[componentID] > 0) {
					if (observationScript == null) {
						System.out.print(
								">> Provide " + nAdditionalObservations[componentID] + " additional observations for "
										+ atoms[1] + " (" + nObservations + " transitions) : ");
						for (int i = 0; i < nObservations; i++) {
							newObservations[i] = sc.nextInt();
						}
					} else {
						runObservationGenerator(newObservations, Integer.toString(nAdditionalObservations[componentID]),
								atoms[1]);
						System.out.print(">> Obtained " + nAdditionalObservations[componentID]
								+ " additional observations for " + atoms[1] + ": ");
						for (int i = 0; i < nObservations; i++) {
							System.out.print(newObservations[i] + " ");
						}
						System.out.println();
					}
				} else {
					System.out.println(">> No additional observations required for " + atoms[1]);
					for (int i = 0; i < nObservations; i++) {
						newObservations[i] = 0;
					}
				}
				String newLine = "";
				for (int i = 0; i < observationsIdx + 2; i++) {
					newLine += atoms[i] + " ";
				}
				int[] totalObs = new int[nObservations];
				for (int i = 0; i < nObservations; i++) {
					totalObs[i] = Integer.parseInt(atoms[observationsIdx + 2 + i]) + newObservations[i];
					newLine += totalObs[i] + " ";
				}

				updateParameterEstimate(parameters, atoms[1], totalObs);
				newLine += "; ";
				for (int i = componentIdx; i < atoms.length; i++) {
					newLine += atoms[i] + " ";
				}
				updatedModel += newLine + "\n";
				System.out.println("  -- " + newLine);
			} else {
				updatedModel += line + "\n";
			}
		}

		data.setModel(updatedModel);
	}

	static private void runObservationGenerator(int[] obs, String nObs, String param) throws Exception {
		String[] commandAndArguments = { observationScript, nObs, param };// for mac
		// String env ="C:\\cygwin64\\bin\\bash.exe ";//for win
		// String[] commandAndArguments = {env,observationScript,nObs,param};//for win
		System.out.println("** Running '" + observationScript + " " + nObs + " " + param + "'");

		ProcessBuilder pb = new ProcessBuilder(commandAndArguments);
		pb.redirectErrorStream(true);

		// start process
		Process proc = pb.start();

		// read process
		String line;
		BufferedReader reader = new BufferedReader(new InputStreamReader(proc.getInputStream()));
		String response = reader.readLine();
		// System.out.println("response: "+response);
		reader.close();
		String[] atoms = response.trim().split("\\s+");
		for (int i = 0; i < obs.length; i++) {
			obs[i] = Integer.parseInt(atoms[i]);
		}
	}

	static private void updateParameterEstimate(Hashtable<String, Parameter> parameters, String paramPrefix,
			int[] obs) {
		double totalObs = 0;
		for (int i = 0; i < obs.length; i++) {
			totalObs += obs[i];
		}
		for (int i = 0; i < obs.length; i++) {
			String paramName = paramPrefix + Integer.toString(i + 1);
			double estimate = ((double) obs[i]) / totalObs;
			parameters.get(paramName).value = estimate;
		}
	}
}
